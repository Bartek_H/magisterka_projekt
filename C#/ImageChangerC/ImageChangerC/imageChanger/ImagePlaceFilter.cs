﻿using ImageChangerC.Threads;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace ImageChangerC.imageChanger
{
    class ImagePlaceFilter
    {
        BitmapImage m_Image = null;
        int m_weight;
        Threads.ThreadPool pool = null;

        public ImagePlaceFilter(BitmapImage image)
        {            
            m_Image = image;
        }

        public void setImage(BitmapImage inputImage)
        {
            m_Image = inputImage;
        }

        public BitmapImage getImage()
        {
            return m_Image;
        }

        private int sumWeight(int[,] filter)
        {
            int retVal = 0;

            if (null != filter)
            {
                for (int i = 0; i < filter.GetLength(0); i++)
                    for (int j = 0; j < filter.GetLength(1); j++)
                        retVal += filter[i, j];
            }

            return retVal;
        }

        public BitmapImage changeImage(int threads, int parts, int[,] filter)
        {
            if (null == pool)
            {
                pool = new Threads.ThreadPool(threads, parts);
            }
            else
            {
                if (threads != pool.getThreadsNum())
                {
                    pool.Dispose();
                    pool = new Threads.ThreadPool(threads, parts);
                }
            }
            TimerManager.TimerManager.getInstance().startTimer(TimerManager.TimerManager.TIMERS_NAME.PREPARE_TASKS);
            List<ThreadPoolActionStorage> taskList =  prepareTasks(parts, filter);
            TimerManager.TimerManager.getInstance().stopTimer(TimerManager.TimerManager.TIMERS_NAME.PREPARE_TASKS);

            TimerManager.TimerManager.getInstance().startTimer(TimerManager.TimerManager.TIMERS_NAME.THREADPOOL);
            System.Console.WriteLine("FOR START");
            for (int i = 0; i < taskList.Count; i++)
            {
                ThreadPoolActionStorage task = taskList[i];
                pool.addTask(() => threadPoolFunc(task));
            }
            System.Console.WriteLine("FOR END");

            pool.waitForTasksToBeDone();
            System.Console.WriteLine("AFTER waitForTasksToBeDone");
            TimerManager.TimerManager.getInstance().stopTimer(TimerManager.TimerManager.TIMERS_NAME.THREADPOOL);

            return mergePreparedTasks(pool.getOutput());
        }

        BitmapImage mergePreparedTasks(List<ThreadPoolOutputStorage> tasks)
        {
            TimerManager.TimerManager.getInstance().startTimer(TimerManager.TimerManager.TIMERS_NAME.SETPIXEL);
            int stride = m_Image.PixelWidth * 4;

            WriteableBitmap bitmap = new WriteableBitmap(m_Image.PixelWidth, m_Image.PixelHeight, m_Image.DpiX, m_Image.DpiY, m_Image.Format, m_Image.Palette);

            int indexToFound = tasks.Count -1;

            for (int i = tasks.Count - 1; i >= 0; i--)
            {
                for (int j = 0; j < tasks.Count; j++)
                {
                    if (tasks[j].index == indexToFound)
                    {
                        if (tasks[j].index != (tasks.Count - 1))
                        {
                            Int32Rect rect = tasks[j].m_rect;
                            rect.Height = rect.Height - 1;
                            tasks[j].m_rect = rect;
                        }
                        bitmap.WritePixels(tasks[j].m_rect, tasks[j].output, stride, 0);
                        
                        indexToFound--;


                    }
                }
            }
            TimerManager.TimerManager.getInstance().stopTimer(TimerManager.TimerManager.TIMERS_NAME.SETPIXEL);

            return ConvertWriteableBitmapToBitmapImage(bitmap);
        }

        List<ThreadPoolActionStorage> prepareTasks(int parts, int [,] filter)
        {
            List<ThreadPoolActionStorage> taskList = new List<ThreadPoolActionStorage>();            
            int weight = sumWeight(filter);
            int height = m_Image.PixelHeight;
            int width = m_Image.PixelWidth;
            Int32Rect rect;

            // 4 bytes per pixel
            int pixelSize = 4;
            int stride = width * pixelSize;

            if (1 == parts)
            {
                byte[] pixels = new byte[stride * height];

                rect = new Int32Rect(0, 0, width, height);

                m_Image.CopyPixels(rect, pixels, stride, 0);

                taskList.Add(new ThreadPoolActionStorage(0, ThreadPoolActionStorage.BORDER_TYPE.BOTH, width, height, weight, pixels, filter, rect));
            }
            else
            {
                int iStartHeightPos = 0;
                int heigthPart = height / parts;
                for (int i = 0; i < parts; i++)
                {
                    //First part of image
                    if (i == 0)
                    {
                        byte[] pixels = new byte[stride * (heigthPart + 1)];
                        rect = new Int32Rect(0, 0, width, heigthPart + 1);
                        m_Image.CopyPixels(rect, pixels, stride, 0);
                        taskList.Add(new ThreadPoolActionStorage(i, ThreadPoolActionStorage.BORDER_TYPE.TOP, width, heigthPart + 1, weight, pixels, filter, rect));
                        iStartHeightPos += heigthPart;
                    }
                    //end part of Image
                    else if ((i + 1) == parts)
                    {
                        byte[] pixels = new byte[stride * (height - iStartHeightPos + 1)];
                        rect = new Int32Rect(0, iStartHeightPos - 1, width, height - iStartHeightPos + 1);
                        m_Image.CopyPixels(rect, pixels, stride, 0);
                        taskList.Add(new ThreadPoolActionStorage(i, ThreadPoolActionStorage.BORDER_TYPE.BOTTOM, width, height - iStartHeightPos + 1, weight, pixels, filter, rect));

                    }                    
                    //Other parts
                    else
                    {
                        byte[] pixels = new byte[stride * (heigthPart + 2)];
                        rect = new Int32Rect(0, iStartHeightPos - 1, width, heigthPart + 2);
                        m_Image.CopyPixels(rect, pixels, stride, 0);
                        taskList.Add(new ThreadPoolActionStorage(i, ThreadPoolActionStorage.BORDER_TYPE.BOTH, width, heigthPart + 2, weight, pixels, filter, rect));
                        iStartHeightPos += heigthPart;
                    }
                }
            }

            return taskList;
        }

        void CreateThumbnail(string filename, BitmapSource image5)
        {
            if (filename != string.Empty)
            {
                using (FileStream stream5 = new FileStream(filename, FileMode.Create))
                {
                    PngBitmapEncoder encoder5 = new PngBitmapEncoder();
                    encoder5.Frames.Add(BitmapFrame.Create(image5));
                    encoder5.Save(stream5);
                }
            }
        }

        public BitmapImage ConvertWriteableBitmapToBitmapImage(WriteableBitmap wbm)
        {
            BitmapImage bmImage = new BitmapImage();
            using (MemoryStream stream = new MemoryStream())
            {
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(wbm));
                encoder.Save(stream);
                bmImage.BeginInit();
                bmImage.CacheOption = BitmapCacheOption.OnLoad;
                bmImage.StreamSource = stream;
                bmImage.EndInit();
                bmImage.Freeze();
            }
            return bmImage;
        }

        Func<ThreadPoolActionStorage, ThreadPoolOutputStorage> threadPoolFunc = (input_data =>
        {
            ThreadPoolActionStorage data = (ThreadPoolActionStorage)input_data;
            byte[] outputpixels = new byte[input_data.m_pixels.Length];
            input_data.m_pixels.CopyTo(outputpixels, 0);
            int width = input_data.m_width;
            int height = input_data.m_height;
            byte[] pixels = input_data.m_pixels;
            int m_weight = input_data.m_weight;
            int[,] filter = input_data.m_imageFilter;

            for (int i = 0; i < pixels.Length; i = i + 4)
            {
                int currentPixel = (i / 4);
                if (0 == (currentPixel % width) || 0 == ((currentPixel + 1) % width))
                {
                    continue;
                }
                else if (currentPixel < width || currentPixel >= (height * width - width))
                {
                    continue;
                }
                else
                {
                    int red = changeColor(pixels, width, i, filter, 2);
                    int green = changeColor(pixels, width, i, filter, 1);
                    int blue = changeColor(pixels, width, i, filter, 0);

                    if (m_weight != 0)
                    {
                        red /= m_weight;
                        green /= m_weight;
                        blue /= m_weight;
                    }
                    if (red > 255) red = 255;
                    if (green > 255) green = 255;
                    if (blue > 255) blue = 255;
                    if (red < 0) red = 0;
                    if (green < 0) green = 0;
                    if (blue < 0) blue = 0;

                    outputpixels[i] = (byte)blue;
                    outputpixels[i + 1] = (byte)green;
                    outputpixels[i + 2] = (byte)red;

                }
            }

            return new ThreadPoolOutputStorage(input_data.m_taskID, outputpixels, input_data.m_rect, (ThreadPoolOutputStorage.BORDER_TYPE)input_data.m_border);
        });

        private static int changeColor(byte [] m_InputPixels, int m_width, int i, int[,] m_imageFilter, int colorShift)
        {
            int pixelWidth = 4;
            m_width = m_width * pixelWidth;
            int color = m_InputPixels[i - pixelWidth - m_width + colorShift] * m_imageFilter[0,0];
            color += m_InputPixels[i - m_width + colorShift] * m_imageFilter[0,1];
            color += m_InputPixels[i + pixelWidth - m_width + colorShift] * m_imageFilter[0,2];

            color += m_InputPixels[i - pixelWidth + colorShift] * m_imageFilter[1,0];
            color += m_InputPixels[i + colorShift] * m_imageFilter[1,1];
            color += m_InputPixels[i + pixelWidth + colorShift] * m_imageFilter[1,2];

            color += m_InputPixels[i - pixelWidth + m_width + colorShift] * m_imageFilter[2,0];
            color += m_InputPixels[i + m_width + colorShift] * m_imageFilter[2,1];
            color += m_InputPixels[i + pixelWidth + m_width + colorShift] * m_imageFilter[2,2];

            return color;
        }
    }
}
