﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageChangerC.imageChanger
{
    class imageBaseFilter
    {
        String m_filterName;
        int[,] m_filter;

        public imageBaseFilter(String name, int[,] filter)
        {
            this.m_filterName = name;
            this.m_filter = filter;
        }

        public int[,] getFilter()
        {
            return m_filter;
        }

        public override string ToString()
        {
            return m_filterName;
        }
    }
}
