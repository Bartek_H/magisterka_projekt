﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageChangerC.imageChanger
{
    class imageFilters
    {
        private static imageFilters m_instnace = null;
        public Dictionary<String, imageBaseFilter> imageFiltersMap = null;

        public static imageFilters getInstance()
        {
            if (null == m_instnace)
            {
                m_instnace = new imageFilters();
            }
            return m_instnace;
        }

        private imageFilters()
        {
            imageFiltersMap = new Dictionary<String, imageBaseFilter>();
            
            imageFiltersMap.Add("HIGH_PASS_FILTER", HIGH_PASS_FILTER);
            imageFiltersMap.Add("LOW_PASS_FILTER", LOW_PASS_FILTER);
            imageFiltersMap.Add("CIRCLE_FILTER", CIRCLE_FILTER);
            imageFiltersMap.Add("GAUSS_FILTER", GAUSS_FILTER);
            imageFiltersMap.Add("MEAN_REMOVAL_FILTER", MEAN_REMOVAL_FILTER);
            imageFiltersMap.Add("EDGE_FILTER_HORIZONTAL_LEFT", EDGE_FILTER_HORIZONTAL_LEFT);
            imageFiltersMap.Add("EDGE_FILTER_HORIZONTAL_UP", EDGE_FILTER_HORIZONTAL_UP);
            imageFiltersMap.Add("GRADIENT_FILTER_EAST", GRADIENT_FILTER_EAST);
            imageFiltersMap.Add("GRADIENT_FILTER_WEST", GRADIENT_FILTER_WEST);
            imageFiltersMap.Add("EMBOSSING_FILTER_EAST", EMBOSSING_FILTER_EAST);
            imageFiltersMap.Add("EMBOSSING_FILTER_WEST", EMBOSSING_FILTER_WEST);
            imageFiltersMap.Add("LAPLACE_FILTER", LAPLACE_FILTER);
            imageFiltersMap.Add("CONTOUR_FILTER_SOBEL_HORIZONTAL", CONTOUR_FILTER_SOBEL_HORIZONTAL);
            imageFiltersMap.Add("CONTOUR_FILTER_SOBEL_VERTICAL", CONTOUR_FILTER_SOBEL_VERTICAL);
        }


        private static imageBaseFilter HIGH_PASS_FILTER = 
            new imageBaseFilter("HIGH_PASS_FILTER", new int[,]{
                                                                {-1, -1, -1},
                                                                {-1, 9, -1},
                                                                {-1, -1, -1}
                                                               });
    
        private static imageBaseFilter LOW_PASS_FILTER = 
            new imageBaseFilter("LOW_PASS_FILTER",new int[,]{
                                                                {1, 1, 1},
                                                                {1, 1, 1},
                                                                {1, 1, 1}
                                                               });
    
        private static imageBaseFilter CIRCLE_FILTER = 
            new imageBaseFilter("CIRCLE_FILTER",new int[,]{
                                                                {0, 1, 0},
                                                                {1, 1, 1},
                                                                {0, 1, 0}
                                                               });
    
    
        private static imageBaseFilter GAUSS_FILTER = 
            new imageBaseFilter("GAUSS_FILTER",new int[,]{
                                                                {1, 2, 1},
                                                                {2, 4, 2},
                                                                {1, 2, 1}
                                                               });
        
        private static imageBaseFilter MEAN_REMOVAL_FILTER = 
            new imageBaseFilter("MEAN_REMOVAL_FILTER",new int[,]{
                                                                {-1, -1, -1},
                                                                {-1, 9, -1},
                                                                {-1, -1, -1}
                                                               });
    
        private static imageBaseFilter EDGE_FILTER_HORIZONTAL_LEFT = 
            new imageBaseFilter("EDGE_FILTER_HORIZONTAL_LEFT",new int[,]{
                                                                {0, 0, 0},
                                                                {-1, 1, 0},
                                                                {0, 0, 0}
                                                               });
    
        private static imageBaseFilter EDGE_FILTER_HORIZONTAL_UP = 
            new imageBaseFilter("EDGE_FILTER_HORIZONTAL_UP",new int[,]{
                                                                {0, -1, 0},
                                                                {0, 1, 0},
                                                                {0, 0, 0}
                                                               });
    
        private static imageBaseFilter GRADIENT_FILTER_EAST = 
            new imageBaseFilter("GRADIENT_FILTER_EAST",new int[,]{
                                                                {-1, 1, 1},
                                                                {-1, -2, 1},
                                                                {-1, 1, 1}
                                                               });
    
        private static imageBaseFilter GRADIENT_FILTER_WEST = 
            new imageBaseFilter("GRADIENT_FILTER_WEST",new int[,]{
                                                                {1, 1, -1},
                                                                {1, -2, -1},
                                                                {1, 1, -1}
                                                               });
    
        private static imageBaseFilter EMBOSSING_FILTER_EAST = 
            new imageBaseFilter("EMBOSSING_FILTER_EAST",new int[,]{
                                                                {-1, 0, 1},
                                                                {-1, 1, 1},
                                                                {-1, 0, 1}
                                                               });
    
        private static imageBaseFilter EMBOSSING_FILTER_WEST = 
            new imageBaseFilter("EMBOSSING_FILTER_WEST",new int[,]{
                                                                {1, 0, -1},
                                                                {1, 1, -1},
                                                                {1, 0, -1}
                                                               });
    
        private static imageBaseFilter LAPLACE_FILTER = 
            new imageBaseFilter("LAPLACE_FILTER",new int[,]{
                                                                {0, -1, 0},
                                                                {-1, 4, -1},
                                                                {0, -1, 0}
                                                               });
    
        private static imageBaseFilter CONTOUR_FILTER_SOBEL_HORIZONTAL = 
            new imageBaseFilter("CONTOUR_FILTER_SOBEL_HORIZONTAL",new int[,]{
                                                                {1, 2, 1},
                                                                {0, 0, 0},
                                                                {-1, -2, -1}
                                                               });
    
        private static imageBaseFilter CONTOUR_FILTER_SOBEL_VERTICAL = 
            new imageBaseFilter("CONTOUR_FILTER_SOBEL_VERTICAL",new int[,]{
                                                                {1, 0, -1},
                                                                {2, 0, -2},
                                                                {1, 0, -1}
                                                               });
    }
}
