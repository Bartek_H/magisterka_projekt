﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ImageChangerC.Threads
{
    public class ThreadPoolOutputStorage
    {
        public byte[] output { get; }
        public int index { get; }
        public Int32Rect m_rect { get; set; }
        public BORDER_TYPE m_border { get; }

        public enum BORDER_TYPE
        {
            TOP,
            BOTTOM,
            BOTH
        }

        public ThreadPoolOutputStorage(int index, byte[] output, Int32Rect rect, BORDER_TYPE border)
        {
            this.index = index;
            this.output = output;
            this.m_rect = rect;
            this.m_border = border;
        }
    }
}
