﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace ImageChangerC.Threads
{
    class ThreadPoolActionStorage
    {
        public int m_taskID { get;}
        public BORDER_TYPE m_border { get;}
        public int m_width { get; }
        public int m_height { get; }
        public int m_weight { get; }
        public byte[] m_pixels { get; }
        public int[,] m_imageFilter { get; }
        public Int32Rect m_rect { get; }

        public enum BORDER_TYPE
        {
            TOP,
            BOTTOM,
            BOTH
        }


        public ThreadPoolActionStorage(int taskID, BORDER_TYPE border, int width, int height, int weight, byte[] pixels, int[,] imageFilter, Int32Rect rect)
        {
            m_taskID = taskID;
            m_border = border;
            m_width = width;
            m_height = height;
            m_weight = weight;
            m_pixels = pixels;
            m_imageFilter = imageFilter;
            m_rect = rect;
        }
    }
}
