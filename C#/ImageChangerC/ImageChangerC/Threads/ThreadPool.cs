﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ImageChangerC.Threads
{
    public sealed class ThreadPool : IDisposable
    {
        private readonly LinkedList<Thread> m_threadPool = null;
        private readonly LinkedList<Func<ThreadPoolOutputStorage>> m_tasksList = null;
        private readonly List<ThreadPoolOutputStorage> m_outputList = null;

        private bool m_disallowToAdd;
        private bool m_clearOutputList;
        private bool m_disposedPoolStarted;
        private int m_threads = 0;

        public ThreadPool(int size, int maxPart = 0)
        {
            m_threads = size;
            m_threadPool = new LinkedList<Thread>();
            m_tasksList = new LinkedList<Func<ThreadPoolOutputStorage>>();
            m_outputList = new List<ThreadPoolOutputStorage>(maxPart);
            m_disallowToAdd = false;
            m_clearOutputList = false;
            m_disposedPoolStarted = false;

            System.Console.WriteLine("NEW ThreadPool");

            for (int i = 0; i < size; ++i)
            {
                Thread thread = new Thread(callBackFunc);
                thread.Name = "Current Thread Id " + i;
                thread.Start();
                m_threadPool.AddLast(thread);
            }
        }

        public int getThreadsNum()
        {
            return m_threads;
        }

        public void waitForTasksToBeDone()
        {
            System.Console.WriteLine("THread: >waitForTasksToBeDone");

            lock (m_tasksList)
            {
                while (m_tasksList.Count > 0)
                {
                    Monitor.Wait(m_tasksList);
                }                
            }

            lock (m_threadPool)
            {
                while (m_threadPool.Count != m_threads)
                {
                    // wait until all threads ends their job
                    Monitor.Wait(m_threadPool);
                }
            }
            
            m_clearOutputList = true;
            System.Console.WriteLine("THread: <waitForTasksToBeDone");
        }

        public void Dispose()
        {
            System.Console.WriteLine("THread: Dispose");
            bool waitForThreads = false;
            lock (m_tasksList)
            {
                if (!m_disposedPoolStarted)
                {
                    GC.SuppressFinalize(this);

                    // wait to clean taskList, disallow to add new tasks.
                    m_disallowToAdd = true; 
                    while (m_tasksList.Count > 0)
                    {
                        Monitor.Wait(m_tasksList);
                    }
                }
            }

            lock (m_threadPool)
            {
                while (m_threadPool.Count != m_threads)
                {
                    // wait until all threads ends their job
                    Monitor.Wait(m_threadPool); 
                }
            }

            lock (m_tasksList)
            {
                m_disposedPoolStarted = true;
                // wake all workers (none of them will be active at this point; disposed flag will cause then to finish so that we can join them)
                Monitor.PulseAll(m_tasksList);
                waitForThreads = true;
            }
        }

        public List<ThreadPoolOutputStorage> getOutput()
        {
            return m_outputList;
        }

        public void addTask(Func<ThreadPoolOutputStorage> task)
        {
            if (m_clearOutputList)
            {
                m_clearOutputList = false;
                m_outputList.Clear();
            }

            lock (m_tasksList)
            {
                if (m_disallowToAdd || m_disposedPoolStarted)
                {
                    //Can not to add, pool is ending.
                }
                else
                {
                    m_tasksList.AddLast(task);
                    Monitor.PulseAll(m_tasksList); // pulse because tasks count changed
                }
            }
        }

        private void callBackFunc()
        {
            Func<ThreadPoolOutputStorage> task = null;
            while (true) // loop until threadpool is disposed
            {
                lock (m_tasksList) // finding a task needs to be atomic
                {
                    while (true) // wait for our turn in _workers queue and an available task
                    {
                        
                        if (m_disposedPoolStarted)
                        {
                            System.Console.WriteLine("THread done: " + Thread.CurrentThread.Name);
                            return;
                        }
                        if (m_tasksList.Count > 0)
                        {
                            task = m_tasksList.First.Value;
                            m_tasksList.RemoveFirst();
                            m_threadPool.Remove(Thread.CurrentThread);
                            Monitor.PulseAll(m_tasksList); // pulse because current (First) worker changed (so that next available sleeping worker will pick up its task)
                            break; // we found a task to process, break out from the above 'while (true)' loop
                        }
                        Monitor.Wait(m_tasksList); // go to sleep, either not our turn or no task to process
                    }
                }


                System.Console.WriteLine("THread start work : " + Thread.CurrentThread.Name);
                ThreadPoolOutputStorage retVal = task();
                System.Console.WriteLine("THread end work : " + Thread.CurrentThread.Name);
                lock (m_outputList) // finding a task needs to be atomic
                {
                    m_outputList.Add(retVal);
                    Monitor.PulseAll(m_outputList);
                }
                lock (m_threadPool) // finding a task needs to be atomic
                {
                    m_threadPool.AddLast(Thread.CurrentThread);
                    Monitor.PulseAll(m_threadPool);
                }
                task = null;
            }
        }
    }
}
