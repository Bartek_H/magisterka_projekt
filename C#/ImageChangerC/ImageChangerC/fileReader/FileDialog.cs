﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Windows.Media.Imaging;
using System.IO;

namespace ImageChangerC.fileReader
{
    class FileDialog
    {
        OpenFileDialog m_OpenDialog;
        SaveFileDialog m_SaveDialog;
        static private string openFileName;
        public FileDialog()
        {
            m_OpenDialog = new OpenFileDialog();
            m_SaveDialog = new SaveFileDialog();

            string filter = "All Graphics Types|*.bmp;*.jpg;*.jpeg;*.png;*.tif;*.tiff";

            m_OpenDialog.Filter = filter;
            m_SaveDialog.Filter = filter;
        }

        public BitmapImage loadFile()
        {

            BitmapImage ob = null;

            Nullable<bool> retVal = m_OpenDialog.ShowDialog();

            if (retVal == true)
            {
                // Open document 
                TimerManager.TimerManager.getInstance().resetTimers();
                openFileName = m_OpenDialog.FileName;
                TimerManager.TimerManager.getInstance().startTimer(TimerManager.TimerManager.TIMERS_NAME.LOAD);
                ob = new BitmapImage(new Uri(openFileName));
                TimerManager.TimerManager.getInstance().stopTimer(TimerManager.TimerManager.TIMERS_NAME.LOAD);
            }

            return ob;
        }

        public void saveFile(BitmapImage image, int threads, int parts, List<String> filterList)
        {
            int startIdx = openFileName.LastIndexOf("\\") + 1;
            m_SaveDialog.FileName = openFileName.Substring(startIdx, openFileName.Length - startIdx);
            Nullable<bool> retVal = m_SaveDialog.ShowDialog();

            if (retVal == true)
            {
                // Open document 
                string filename = m_SaveDialog.FileName;
                
                if (filename != string.Empty)
                {
                    TimerManager.TimerManager.getInstance().startTimer(TimerManager.TimerManager.TIMERS_NAME.SAVE);
                    using (FileStream stream5 = new FileStream(filename, FileMode.Create))
                    {
                        PngBitmapEncoder encoder5 = new PngBitmapEncoder();
                        encoder5.Frames.Add(BitmapFrame.Create(image));
                        encoder5.Save(stream5);
                        
                    }
                    TimerManager.TimerManager.getInstance().stopTimer(TimerManager.TimerManager.TIMERS_NAME.SAVE);

                    string fileNameWIthotextension = filename.Substring(0, filename.LastIndexOf("."));
                    string timeText =
                        "THREADS : " + threads +
                        "\nTPARTS : " + parts +
                        "\nLOAD TIME: " + TimerManager.TimerManager.getInstance().getTime(TimerManager.TimerManager.TIMERS_NAME.LOAD) +
                        " ms\nPREPARE TASKS TIME: " + TimerManager.TimerManager.getInstance().getTime(TimerManager.TimerManager.TIMERS_NAME.PREPARE_TASKS) +
                        " ms\nTHREADPOOL TIME: " + TimerManager.TimerManager.getInstance().getTime(TimerManager.TimerManager.TIMERS_NAME.THREADPOOL) +
                        " ms\nSETPIXEL TIME: " + TimerManager.TimerManager.getInstance().getTime(TimerManager.TimerManager.TIMERS_NAME.SETPIXEL) +
                        " ms\nSAVE TIME: " + TimerManager.TimerManager.getInstance().getTime(TimerManager.TimerManager.TIMERS_NAME.SAVE) +
                        " ms\nOVERALL TIME: " + TimerManager.TimerManager.getInstance().getTime(TimerManager.TimerManager.TIMERS_NAME.OVERALL_TIME) +
                        " \nFILTER USED: \n";

                    for (int i = 0; i < filterList.Count; i++)
                    {
                        timeText += filterList[i] + "\n";
                    }

                    File.WriteAllText(fileNameWIthotextension + "_REPORT.txt", timeText);
                }
                
            }

        }
    }
}
