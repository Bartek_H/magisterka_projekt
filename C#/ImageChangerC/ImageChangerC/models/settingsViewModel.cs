﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace ImageChangerC.models
{
    class settingsViewModel
    {
        public settingsViewModel()
        {
            m_OutputImagePage = null;
            m_image = null;
        }

        public OutputImagePage m_OutputImagePage { get; set; }
        public BitmapImage m_image { get; set; }
    }
}
