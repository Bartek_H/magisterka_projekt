﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImageChangerC
{
    /// <summary>
    /// Interaction logic for OutputImagePage.xaml
    /// </summary>
    public partial class OutputImagePage : Page
    {
        public OutputImagePage()
        {
            InitializeComponent();
        }

        private BitmapImage img;
        public BitmapImage Img
        {
            get
            {
                return this.img;
            }
            set
            {
                this.img = value;
                this.ImageHolder.Source = this.img;
            }
        }


    }
}
