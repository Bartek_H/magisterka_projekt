﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImageChangerC
{
    /// <summary>
    /// Interaction logic for LoadImagePage.xaml
    /// </summary>
    public partial class LoadImagePage : Page
    {

        BitmapImage internalImage = null;
        public LoadImagePage()
        {
            InitializeComponent();
        }

        public void LoadImage()
        {
            fileReader.FileDialog ob = new fileReader.FileDialog();
            internalImage = ob.loadFile();

            if (null != internalImage)
            {
                this.ImageHolder.Source = internalImage;
                this.MiddleText.Visibility = Visibility.Hidden;
            }
            else
            {
                this.MiddleText.Visibility = Visibility.Visible;
            }
        }

        public BitmapImage getImage()
        {
            return internalImage;
        }

        public void showImage()
        {
            if (null != internalImage)
            {
                this.ImageHolder.Source = internalImage;
                this.MiddleText.Visibility = Visibility.Hidden;
            }
            else
            {
                this.MiddleText.Visibility = Visibility.Visible;
            }
        }
    }
}
