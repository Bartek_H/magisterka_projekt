﻿using ImageChangerC.imageChanger;
using ImageChangerC.models;
using ImageChangerC.Threads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImageChangerC.Pages
{
    /// <summary>
    /// Interaction logic for SettingsPage.xaml
    /// </summary>
    public partial class SettingsPage : Page
    {
        private class TaskClass
        {
            public enum WORK_TYPE
            {
                IMAGE_PROCESS,
                SAVE
            }

            public WORK_TYPE m_type { get; set; }
            public BitmapImage m_img { get; set; }

            public TaskClass(WORK_TYPE type, BitmapImage img)
            {
                m_type = type;
                m_img = img;
            }
        }
        imageFilters m_filter = null;
        settingsViewModel m_internalModel = null;
        ImagePlaceFilter filter = null;



        public SettingsPage()
        {
            InitializeComponent();
            m_filter = imageFilters.getInstance();
            //m_workThread = new Thread(workThreadCallBack);
            filter = new ImagePlaceFilter(null);

            if (null != m_filter)
            {  
                  this.oFilterComboBox.ItemsSource =  m_filter.imageFiltersMap.Values.ToList();                
            }

            oListBox.Items.Add("GRADIENT_FILTER_WEST");
            oListBox.Items.Add("CONTOUR_FILTER_SOBEL_HORIZONTAL");
            oListBox.Items.Add("GRADIENT_FILTER_EAST");
            oListBox.Items.Add("GAUSS_FILTER");
            oListBox.Items.Add("EMBOSSING_FILTER_WEST");
            oListBox.Items.Add("EMBOSSING_FILTER_EAST");
            oListBox.Items.Add("HIGH_PASS_FILTER");
            oListBox.Items.Add("LAPLACE_FILTER");
            oListBox.Items.Add("EDGE_FILTER_HORIZONTAL_UP");
            oListBox.Items.Add("LOW_PASS_FILTER");
        }

        private void oAddButton_Click(object sender, RoutedEventArgs e)
        {
            if (null != this.oFilterComboBox.SelectedItem)
            {
                String filterName = this.oFilterComboBox.SelectedItem.ToString();
                oListBox.Items.Add(filterName);
            }
        }

        private void oRemoveLast_Click(object sender, RoutedEventArgs e)
        {
            if (0 != oListBox.Items.Count)
            {
                oListBox.Items.RemoveAt(oListBox.Items.Count - 1);
            }
        }

        private void oRemoveAll_Click(object sender, RoutedEventArgs e)
        {
            while(0 != oListBox.Items.Count)
            {
                oListBox.Items.RemoveAt(0);
            }
        }

        private void oStartButton_Click(object sender, RoutedEventArgs e)
        {
            //TaskClass task = new TaskClass(TaskClass.WORK_TYPE.IMAGE_PROCESS, m_internalModel.m_image);
            //m_workThread.Start(task);

            bool isOk = true;
            int threadNum = 1;
            int partNum = 1;

            if (isOk && !Int32.TryParse(oThreadsNumber.Text, out threadNum))
            {
                isOk = false;
            }

            if (isOk && !Int32.TryParse(oPartsNumber.Text, out partNum))
            {
                isOk = false;
            }

            isOk = (null != m_internalModel) && (null != m_internalModel.m_image);

            if (isOk)
            {
                oProgressBar.Maximum = oListBox.Items.Count;
                oProgressBar.Minimum = 0;

                BitmapImage img = null;
                
                for (int i = 0; i < oListBox.Items.Count; i++)
                {
                    oProgressBar.Value = i+1;
                    img = filter.changeImage(threadNum, partNum, imageFilters.getInstance().imageFiltersMap[oListBox.Items.GetItemAt(i).ToString()].getFilter());
                    filter.setImage(img);
                }
                m_internalModel.m_OutputImagePage.Img = filter.getImage();
            }

        }

        private void oSaveOutputButton_Click(object sender, RoutedEventArgs e)
        {
            bool isOk = true;
            int threadNum = 1;
            int partNum = 1;

            if (isOk && !Int32.TryParse(oThreadsNumber.Text, out threadNum))
            {
                isOk = false;
            }

            if (isOk && !Int32.TryParse(oPartsNumber.Text, out partNum))
            {
                isOk = false;
            }

            List<String> filterList = new List<String>();

            for (int i = 0; i < oListBox.Items.Count; i++)
            {
                filterList.Add(oListBox.Items.GetItemAt(i).ToString());
                
            }


            if (isOk && (null != m_internalModel) && (null != m_internalModel.m_OutputImagePage) && (null != m_internalModel.m_OutputImagePage.Img))
            {
                fileReader.FileDialog ob = new fileReader.FileDialog();
                ob.saveFile(m_internalModel.m_OutputImagePage.Img, threadNum , partNum, filterList);
            }
        }

        private void Grid_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            settingsViewModel model = (settingsViewModel)this.DataContext;
            if (null != model)
            {
                m_internalModel = model;
                filter.setImage(m_internalModel.m_image);
            }
        }

        private void oRefreshButton_Click(object sender, RoutedEventArgs e)
        {
            if (null != m_internalModel)
            {
                TimerManager.TimerManager.getInstance().resetTimers();
                filter.setImage(m_internalModel.m_image);
            }
        }

        //public void workThreadCallBack(object type)
        //{
        //    if (type is TaskClass)
        //    {
        //        TaskClass task = (TaskClass)type;
        //        switch (task.m_type)
        //        {
        //            case TaskClass.WORK_TYPE.IMAGE_PROCESS:
        //            {

        //                break;
        //            }
        //            case TaskClass.WORK_TYPE.SAVE:
        //            {
        //                break;
        //            }
        //        };
        //    }
        //    else
        //    {
        //        //nothing to do
        //    }
        //}
    }
}
