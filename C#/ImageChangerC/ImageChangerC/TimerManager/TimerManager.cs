﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageChangerC.TimerManager
{
    public class TimerManager
    {
        public enum TIMERS_NAME
        {
            LOAD = 0,
            PREPARE_TASKS,
            THREADPOOL,
            SETPIXEL,
            SAVE,
            OVERALL_TIME,
            TIMER_SIZE
        };

        List<Stopwatch> timerList;
        static TimerManager instnace;

        Stopwatch myTime;
        double overAllTIme = 0;



        protected TimerManager()
        {
            timerList = new List<Stopwatch>((int)TIMERS_NAME.TIMER_SIZE);
            for (int i = 0; i < (int)TIMERS_NAME.TIMER_SIZE; i++)
            {
                timerList.Add(new Stopwatch());
            }
        }

        public static TimerManager getInstance()
        {
            if (null == instnace)
            {
                instnace = new TimerManager();
            }
            return instnace;
        }

        public void startTimer(TIMERS_NAME timer)
        {
            timerList[(int)timer].Start();
        }

        public int stopTimer(TIMERS_NAME timer)
        {
            timerList[(int)timer].Stop();
            return (int)(timerList[(int)timer].ElapsedMilliseconds);
        }

        public void resetTimers()
        {
            for (int i = 0; i < (int)TIMERS_NAME.TIMER_SIZE; i++)
            {
                timerList[i].Reset();
            }
        }

        public int getTime(TIMERS_NAME timer)
        {
            
            if (TIMERS_NAME.OVERALL_TIME == timer)
            {
                double time = 0;
                for (int i = 0; i < (int)TIMERS_NAME.OVERALL_TIME; i++)
                {
                    time += timerList[i].ElapsedMilliseconds;
                }
                return (int)(time);
            }
            else
            {
                return (int)(timerList[(int)timer].ElapsedMilliseconds);
            }
        }
    }
}
