﻿using ImageChangerC.imageChanger;
using ImageChangerC.models;
using ImageChangerC.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImageChangerC
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private LoadImagePage m_LoadImagePage = null;
        private OutputImagePage m_OutputImagePage = null;
        private SettingsPage m_SettingsPage = null;

        public MainWindow()
        {
            InitializeComponent();

            m_LoadImagePage = new LoadImagePage();
            m_OutputImagePage = new OutputImagePage();
            m_SettingsPage = new SettingsPage();



        }

        private void BtnPage1(object sender, RoutedEventArgs e)
        {
            m_LoadImagePage.LoadImage();
            Main.Content = m_LoadImagePage;
            settingsViewModel settingsModel = new settingsViewModel();
            settingsModel.m_image = m_LoadImagePage.getImage();
            settingsModel.m_OutputImagePage = m_OutputImagePage;
            m_SettingsPage.DataContext = settingsModel;
        }

        private void BtnPage2(object sender, RoutedEventArgs e)
        {
            Main.Content = m_LoadImagePage;
        }

        private void BtnPage3(object sender, RoutedEventArgs e)
        {
            Main.Content = m_OutputImagePage;
        }

        private void BtnPage4(object sender, RoutedEventArgs e)
        {
            Main.Content = m_SettingsPage;
        }
    }
}
