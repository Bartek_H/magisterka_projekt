/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imagechanger;

import static imagechanger.ImageTimerManager.LAST_TIMER;
import static imagechanger.ImageTimerManager.LOAD_TIMER;
import static imagechanger.ImageTimerManager.SAVE_TIMER;
import static imagechanger.ImageTimerManager.SET_PIXELS_TIMER;
import static imagechanger.ImageTimerManager.TASK_PREPARE_TIMER;
import static imagechanger.ImageTimerManager.THREADPOOL_TIMER;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bartosz Hajdaś <bartekkh@gmail.com>
 */
public class imageReportGenerator {

    public imageReportGenerator(File a_file,int a_threads, int a_parts, ArrayList<String> filters) 
    {
        String pathToDir = a_file.getParent() ;
        String fileName = a_file.getName().substring(0, a_file.getName().lastIndexOf("."));
        fileName +=+ a_threads+ "w"+ a_parts   +"p_report.txt";


        System.err.println("");
        BufferedWriter writer;
        try 
        {
            writer = new BufferedWriter(new FileWriter(pathToDir + "\\" + fileName));
            
            writer.write("THREADS NUMBER = " + String.valueOf(a_threads) + "\n");
            writer.write("PARTS NUMBER = " + String.valueOf(a_parts) + "\n");
            
            int time = 0;
            for(int i = 0; i < LAST_TIMER; i++)
            {
                time += ImageTimerManager.getInstance().getTime(i);
            }
            
            writer.write("LOAD TIME = " + ImageTimerManager.getInstance().getTimeAsString(LOAD_TIMER) + "\n");
            writer.write("TASK PREPARE TIME = " + ImageTimerManager.getInstance().getTimeAsString(TASK_PREPARE_TIMER) + "\n");
            writer.write("THREADPOOL TIME = " + ImageTimerManager.getInstance().getTimeAsString(THREADPOOL_TIMER) + "\n");
            writer.write("SET PIXEL TIME = " + ImageTimerManager.getInstance().getTimeAsString(SET_PIXELS_TIMER) + "\n");
            writer.write("SAVE TIME = " + ImageTimerManager.getInstance().getTimeAsString(SAVE_TIMER) + "\n");
            writer.write("OVERALL TIME = " + String.valueOf(time) + "\n");
            
            writer.write("FILTERS USED : \n");
            
            for(int i = 0; i < filters.size(); i++)
            {
                writer.write(filters.get(i) + "\n");
            }
            
            writer.close();
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(imageReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }

        
    }
    
    
    
}
