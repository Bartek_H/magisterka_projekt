/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imagechanger;

import imageGUI.MainWindow;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Bartosz Hajdaś <bartekkh@gmail.com>
 */
public class ImageChanger {

    /**
     * Set look and feel
     */
    private static void setUI()
    {
        try 
        {           
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } 
        catch (Exception e) {
           // handle exception
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
        public void run() {
            setUI();
            MainWindow oWindow = new MainWindow();
            oWindow.setVisible(true);
        }
    });

    }
    
}
