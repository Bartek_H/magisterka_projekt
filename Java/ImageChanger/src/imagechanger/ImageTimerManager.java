/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imagechanger;

import java.util.ArrayList;

/**
 *
 * @author Bartosz Hajdaś <bartekkh@gmail.com>
 */
public class ImageTimerManager {

    public static final int LOAD_TIMER = 0;
    public static final int TASK_PREPARE_TIMER = 1;
    public static final int THREADPOOL_TIMER = 2;
    public static final int SET_PIXELS_TIMER = 3;
    public static final int SAVE_TIMER = 4;
    public static final int LAST_TIMER = 5;
    private ArrayList<TimerMeasure> timerTable = null;
    private static ImageTimerManager m_instance = null;
    
    private class TimerMeasure
    {
        long startTime = 0;
        long endTime = 0;
        long allTime = 0;
        
        public void startTime()
        {
            startTime = System.nanoTime();
        }
        
        public long endTime()
        {
            endTime = System.nanoTime();
            allTime += endTime - startTime;
            return endTime - startTime;
        }
        
        public long getTime()
        {
            return allTime  / 1000000;
        }
        
        public String getTimeAsString()
        {            
            return String.valueOf(allTime / 1000000);
        }
        
        public void reset()
        {
            allTime = 0;
            startTime = 0;
            endTime = 0;
        }
    }
    
    protected ImageTimerManager() {
        timerTable = new ArrayList<>();
        
        for (int i = 0; i < LAST_TIMER; i++) {
            timerTable.add(new TimerMeasure());
        }        
    }
    
    public static ImageTimerManager getInstance()
    {
        if(null == m_instance)
        {
            m_instance = new ImageTimerManager();
        }
        
        return m_instance;
    }
    
    public void  startTimer(int timerId)
    {
        timerTable.get(timerId).startTime();
    }
    
    public long endTimer(int timerId)
    {
        return timerTable.get(timerId).endTime();
    }
    
    public long getTime(int timerId)
    {
        return timerTable.get(timerId).getTime();
    }
    
    public String getTimeAsString(int timerId)
    {
        return timerTable.get(timerId).getTimeAsString();
    }
    
    public long getOverallTime()
    {
        long time = 0;
        
        for (int i = 0; i < timerTable.size(); i++) {
            time += timerTable.get(i).getTime();            
        }
        
        return time;
    }
    
    public void resetTimers()
    {
        for (int i = 0; i < timerTable.size(); i++) {
            timerTable.get(i).reset();            
        }    
    }
}
