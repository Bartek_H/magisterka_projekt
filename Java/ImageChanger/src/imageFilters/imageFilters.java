/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageFilters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Bartosz Hajdaś <bartekkh@gmail.com>
 */
public class imageFilters {
    
    private ArrayList<imageBaseFilter> filterList;
    private static imageFilters m_instnace = null;
    Map imageFiltersMap = null;

    
    public static imageFilters getInstance()
    {
        if(null == m_instnace)
        {
            m_instnace = new imageFilters();
        }
        return m_instnace;
    }
    
    
    private imageFilters()
    {
        imageFiltersMap = new HashMap<String,imageBaseFilter>();
        
        imageFiltersMap.put("HIGH_PASS_FILTER", HIGH_PASS_FILTER);
        imageFiltersMap.put("LOW_PASS_FILTER", LOW_PASS_FILTER);
        imageFiltersMap.put("CIRCLE_FILTER", CIRCLE_FILTER);     
        imageFiltersMap.put("GAUSS_FILTER", GAUSS_FILTER);     
        imageFiltersMap.put("MEAN_REMOVAL_FILTER", MEAN_REMOVAL_FILTER);          
        imageFiltersMap.put("EDGE_FILTER_HORIZONTAL_LEFT", EDGE_FILTER_HORIZONTAL_LEFT);
        imageFiltersMap.put("EDGE_FILTER_HORIZONTAL_UP", EDGE_FILTER_HORIZONTAL_UP);
        imageFiltersMap.put("GRADIENT_FILTER_EAST", GRADIENT_FILTER_EAST);
        imageFiltersMap.put("GRADIENT_FILTER_WEST", GRADIENT_FILTER_WEST);
        imageFiltersMap.put("EMBOSSING_FILTER_EAST", EMBOSSING_FILTER_EAST);
        imageFiltersMap.put("EMBOSSING_FILTER_WEST", EMBOSSING_FILTER_WEST);
        imageFiltersMap.put("LAPLACE_FILTER", LAPLACE_FILTER);
        imageFiltersMap.put("CONTOUR_FILTER_SOBEL_HORIZONTAL", CONTOUR_FILTER_SOBEL_HORIZONTAL);
        imageFiltersMap.put("CONTOUR_FILTER_SOBEL_VERTICAL", CONTOUR_FILTER_SOBEL_VERTICAL);
    }
    
    public Map getFilters()
    {
        return imageFiltersMap;
    }
    
    private static final imageBaseFilter HIGH_PASS_FILTER = 
            new imageBaseFilter("HIGH_PASS_FILTER",new int [][]{
                                                                {-1, -1, -1},
                                                                {-1, 9, -1},
                                                                {-1, -1, -1}
                                                               });
    
    private static final imageBaseFilter LOW_PASS_FILTER = 
        new imageBaseFilter("LOW_PASS_FILTER",new int [][]{
                                                            {1, 1, 1},
                                                            {1, 1, 1},
                                                            {1, 1, 1}
                                                           });
    
    private static final imageBaseFilter CIRCLE_FILTER = 
        new imageBaseFilter("CIRCLE_FILTER",new int [][]{
                                                            {0, 1, 0},
                                                            {1, 1, 1},
                                                            {0, 1, 0}
                                                           });
    
    
    private static final imageBaseFilter GAUSS_FILTER = 
        new imageBaseFilter("GAUSS_FILTER",new int [][]{
                                                            {1, 2, 1},
                                                            {2, 4, 2},
                                                            {1, 2, 1}
                                                           });
        
    private static final imageBaseFilter MEAN_REMOVAL_FILTER = 
        new imageBaseFilter("MEAN_REMOVAL_FILTER",new int [][]{
                                                            {-1, -1, -1},
                                                            {-1, 9, -1},
                                                            {-1, -1, -1}
                                                           });
    
    private static final imageBaseFilter EDGE_FILTER_HORIZONTAL_LEFT = 
        new imageBaseFilter("EDGE_FILTER_HORIZONTAL_LEFT",new int [][]{
                                                            {0, 0, 0},
                                                            {-1, 1, 0},
                                                            {0, 0, 0}
                                                           });
    
    private static final imageBaseFilter EDGE_FILTER_HORIZONTAL_UP = 
        new imageBaseFilter("EDGE_FILTER_HORIZONTAL_UP",new int [][]{
                                                            {0, -1, 0},
                                                            {0, 1, 0},
                                                            {0, 0, 0}
                                                           });
    
    private static final imageBaseFilter GRADIENT_FILTER_EAST = 
        new imageBaseFilter("GRADIENT_FILTER_EAST",new int [][]{
                                                            {-1, 1, 1},
                                                            {-1, -2, 1},
                                                            {-1, 1, 1}
                                                           });
    
    private static final imageBaseFilter GRADIENT_FILTER_WEST = 
        new imageBaseFilter("GRADIENT_FILTER_WEST",new int [][]{
                                                            {1, 1, -1},
                                                            {1, -2, -1},
                                                            {1, 1, -1}
                                                           });
    
    private static final imageBaseFilter EMBOSSING_FILTER_EAST = 
        new imageBaseFilter("EMBOSSING_FILTER_EAST",new int [][]{
                                                            {-1, 0, 1},
                                                            {-1, 1, 1},
                                                            {-1, 0, 1}
                                                           });
    
    private static final imageBaseFilter EMBOSSING_FILTER_WEST = 
        new imageBaseFilter("EMBOSSING_FILTER_WEST",new int [][]{
                                                            {1, 0, -1},
                                                            {1, 1, -1},
                                                            {1, 0, -1}
                                                           });
    
    private static final imageBaseFilter LAPLACE_FILTER = 
        new imageBaseFilter("LAPLACE_FILTER",new int [][]{
                                                            {0, -1, 0},
                                                            {-1, 4, -1},
                                                            {0, -1, 0}
                                                           });
    
    private static final imageBaseFilter CONTOUR_FILTER_SOBEL_HORIZONTAL = 
        new imageBaseFilter("CONTOUR_FILTER_SOBEL_HORIZONTAL",new int [][]{
                                                            {1, 2, 1},
                                                            {0, 0, 0},
                                                            {-1, -2, -1}
                                                           });
    
    private static final imageBaseFilter CONTOUR_FILTER_SOBEL_VERTICAL = 
        new imageBaseFilter("CONTOUR_FILTER_SOBEL_VERTICAL",new int [][]{
                                                            {1, 0, -1},
                                                            {2, 0, -2},
                                                            {1, 0, -1}
                                                           });
}
