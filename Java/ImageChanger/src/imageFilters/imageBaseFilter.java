/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageFilters;

/**
 *
 * @author Bartosz Hajdaś <bartekkh@gmail.com>
 */
public class imageBaseFilter  {

    String m_filterName;
    int [][] m_filter;
    
    public imageBaseFilter(String name, int [][] filter) {
        this.m_filterName = name;
        this.m_filter = filter;
    }
    
    public int [][] getFilter()
    {
        return m_filter;
    }

    @Override
    public String toString() {
        return m_filterName;
    }
    
    
    
    
}
