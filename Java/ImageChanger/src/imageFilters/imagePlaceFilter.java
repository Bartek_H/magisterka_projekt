/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageFilters;

import imageThreadPool.imageThreadPool;
import imageThreadPool.imageThreadPoolTask;
import static imageThreadPool.imageThreadPoolTask.ONE_BORDER;
import static imageThreadPool.imageThreadPoolTask.TWO_BORDERS;
import static imageThreadPool.imageThreadPoolTask.ZERO_BORDER;
import imagechanger.ImageTimerManager;
import static imagechanger.ImageTimerManager.SET_PIXELS_TIMER;
import static imagechanger.ImageTimerManager.TASK_PREPARE_TIMER;
import static imagechanger.ImageTimerManager.THREADPOOL_TIMER;
import java.awt.Image;
import java.awt.image.BufferedImage;

/**
 *
 * @author Bartosz Hajdaś <bartekkh@gmail.com>
 */
public class imagePlaceFilter {

    /**
     * Handle to image
     */
    private Image m_Image = null;
    private imageThreadPool pool = null;
    
    public imagePlaceFilter(Image inputImage) {
        m_Image = inputImage;        
    }
    
    public void setImage(Image inputImage)
    {
        m_Image = inputImage;
    }
    
    /**
     * Calculate weigth for given fiter
     * @param filter
     * @return 
     */
    private int sumWeight(int [][] filter)
    {
        int retVal  = 0;
        
        if(null != filter )
        {            
            for(int [] rows : filter)
            {
                for(int values : rows)
                {
                    retVal += values;
                }
            }   
        }
        
        return retVal;
    }
    /**
     * @return changed image 
     * @brief method change given image based on num of thread and filter
     * @param threads threads to use in threadPool
     * @param parts part for threadPool
     * @param filter given filter - see imageFilter.java
     */
    public BufferedImage changeImage(int threads, int parts, int [][]filter)
    {     
        BufferedImage oBufferedImg = null;
        if((0 >= threads) || (0 >= parts) || (null == filter))
        {
            System.err.println("Wrong arguments");
        }
        else
        {        
            oBufferedImg = (BufferedImage) m_Image;
            int iWidth = oBufferedImg.getWidth();
            int iHeigth = oBufferedImg.getHeight();
            if(null == pool)
            {
                pool = new imageThreadPool(threads);
            }
            else
            {
                if(threads != pool.getThreads())
                {
                    pool = new imageThreadPool(threads);
                }
            }

            ImageTimerManager.getInstance().startTimer(TASK_PREPARE_TIMER);
            imageThreadPoolTask [] tasks = prepareTasks(oBufferedImg, parts, iWidth, iHeigth, filter);
            ImageTimerManager.getInstance().endTimer(TASK_PREPARE_TIMER);

            ImageTimerManager.getInstance().startTimer(THREADPOOL_TIMER);
            for (int i = 0; i < tasks.length; i++) 
            {
                 pool.doTask(tasks[i]);            
            }
            pool.waitForTaskDone();
            ImageTimerManager.getInstance().endTimer(THREADPOOL_TIMER);

            ImageTimerManager.getInstance().startTimer(SET_PIXELS_TIMER);
            setRGBInsideImage(oBufferedImg, tasks, iWidth, iHeigth);  
            ImageTimerManager.getInstance().endTimer(SET_PIXELS_TIMER);
        }
        
        return oBufferedImg;
    }
    
    /**
     * 
     * @return given image
     */
    public BufferedImage getImage()
    {
        return (BufferedImage) m_Image;
    }
    
    /**
     * 
     * @param oBufferedImg given image
     * @param parts parts for threadPool
     * @param iWidth 
     * @param iHeigth
     * @param filter
     * @return task for threadPool
     */
    private imageThreadPoolTask [] prepareTasks(BufferedImage oBufferedImg, int parts, int iWidth, int iHeigth, int [][] filter )
    {
        int iWeight = sumWeight(filter);        
        int iStartWidthPos = 0;
        int iStartHeightPos = 0;
        imageThreadPoolTask [] tasks = new imageThreadPoolTask[parts];
        int heigthPart = iHeigth / tasks.length;        
        
        if(1 == tasks.length)
        {
            int [] pixels = new int [iWidth * iHeigth];
            oBufferedImg.getRGB(iStartWidthPos, iStartHeightPos, iWidth , iHeigth , pixels,0 ,iWidth);
            tasks[0] = new imageThreadPoolTask(0,ZERO_BORDER,iWidth, iWeight, pixels, filter);  
        }
        else
        {        
            for(int i = 0; i < tasks.length; i++)
            {
                //end part of Image
                if((i + 1) == tasks.length)
                {
                    // (iHeigth - iStartHeightPos + 1) Calculate heigth to end of image
                    int [] pixels = new int [iWidth * (iHeigth - iStartHeightPos + 1)];
                    oBufferedImg.getRGB(iStartWidthPos, iStartHeightPos - 1, iWidth , iHeigth - iStartHeightPos + 1 , pixels,0 ,iWidth);
                    tasks[i] = new imageThreadPoolTask(i,ONE_BORDER,iWidth, iWeight, pixels, filter);  
                }
                //First part of image
                else if ( i == 0)
                {
                    int [] pixels = new int [iWidth * (heigthPart + 1)];
                    oBufferedImg.getRGB(iStartWidthPos, iStartHeightPos, iWidth , heigthPart + 1 , pixels,0 ,iWidth);
                    tasks[i] = new imageThreadPoolTask(i,ONE_BORDER,iWidth, iWeight, pixels, filter);
                    iStartHeightPos += heigthPart;
                }
                //Other parts
                else
                {
                    int [] pixels = new int [iWidth * (heigthPart + 2)];
                    oBufferedImg.getRGB(iStartWidthPos, iStartHeightPos - 1, iWidth , heigthPart + 2 , pixels,0 ,iWidth);
                    iStartHeightPos += heigthPart;
                    tasks[i] = new imageThreadPoolTask(i,TWO_BORDERS,iWidth, iWeight, pixels, filter);  
                }
            }   
        }
        return tasks;
    }
    
    /**
     * 
     * @param oBufferedImg given image
     * @param tasks tasks from ThreadPool
     * @param parts number of tasks;
     * @param iWidth
     * @param iHeigth 
     */
    private void setRGBInsideImage(BufferedImage oBufferedImg, imageThreadPoolTask [] tasks, int iWidth, int iHeigth)
    {
        int iStartWidthPos = 0;
        int iStartHeightPos = 0;
        int heigthPart = iHeigth / tasks.length;
        
        
        if(1 == tasks.length)
        {
            oBufferedImg.setRGB(iStartWidthPos, iStartHeightPos, iWidth , iHeigth, tasks[0].getOutputPixels(),0 ,iWidth);
            iStartHeightPos += heigthPart;
        }
        else
        {
            for(int i = 0; i < tasks.length; i++)
            {
                //Last part of image
                if((i + 1) == tasks.length)
                {
                    // (iHeigth - iStartHeightPos - 1) Calculate heigth to end of image
                    oBufferedImg.setRGB(iStartWidthPos, iStartHeightPos, iWidth , (iHeigth - iStartHeightPos - 1), tasks[i].getOutputPixels(),0 ,iWidth);
                    iStartHeightPos += heigthPart;
                }
                //First part of image
                else if ( i == 0)
                {
                    oBufferedImg.setRGB(iStartWidthPos, iStartHeightPos + 1 , iWidth  , heigthPart , tasks[i].getOutputPixels(),0 ,iWidth);
                    iStartHeightPos += heigthPart;
                }
                //Other parts
                else
                {
                    oBufferedImg.setRGB(iStartWidthPos, iStartHeightPos , iWidth  , heigthPart , tasks[i].getOutputPixels(),0 ,iWidth);
                    iStartHeightPos += heigthPart;
                }               
            }
        }
    }
}
