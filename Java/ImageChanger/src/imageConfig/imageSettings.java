/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageConfig;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bartosz Hajdaś <bartekkh@gmail.com>
 */
public class imageSettings 
{    
    
    //PROPERTIES
    final public static String OPEN_DIALOG_BOX_PATH = "openDialogBoxPath";
    
    //CONSTS
    final private static String PROPERTIES_FILE = "config.properties";
    
    //MEMBERS
    private static Properties m_Prop = null;
    private static OutputStream m_Output = null;
    private static InputStream m_Input = null;
    private static imageSettings m_Instance = null;

    /**
     * Default ctor
     */
    protected imageSettings() 
    {
        m_Prop = new Properties();
    }
    
    /**
     * getter to singleton
     * @return instnace
     */
    public static imageSettings getInstance()
    {
        if(null == m_Instance)
        {
            m_Instance = new imageSettings();
        }
        
        return m_Instance;
    }
    
    /**
     * Reset/delete propertys file
     * @return true on success
     */    
    public boolean resetPropertys()
    {        
        boolean retVal = true;
        try 
        {        
            Path oPath = Paths.get(PROPERTIES_FILE);
            if(Files.exists(oPath))
            {
                Files.delete(oPath);
            }
        } 
        catch (IOException ex)
        {
            Logger.getLogger(imageSettings.class.getName()).log(Level.SEVERE, null, ex);
            retVal = false;
        }
        
        return retVal;
    }

    /**
     * @brief Set property inside property file
     * @param property
     * @param value
     * @return true on success
     */
    public boolean setProperty(String property, String value)
    {
        boolean retVal = true;

        try 
        {
            m_Output = new FileOutputStream(PROPERTIES_FILE);

            m_Prop.setProperty(property, value);

            m_Prop.store(m_Output, null);

            m_Output.close();
        } 
        catch (IOException io) 
        {
                io.printStackTrace();
                retVal = false;
        }

        return retVal;
    }

    /**
     * 
     * @param property to read
     * @return non-null string
     */    
    public String loadProperty(String property)
    {
        String loadedProperty = " ";

        try 
        {
            m_Input = new FileInputStream(PROPERTIES_FILE);

            m_Prop.load(m_Input);
            
            loadedProperty = m_Prop.getProperty(property);
            if(null == loadedProperty)
            {
                loadedProperty = new String();
            }

            m_Input.close();
        } 
        catch (IOException io) 
        {
                io.printStackTrace();
        }

        return loadedProperty;
    }   
}
