/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageReader;


import java.awt.Component;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.image.ImageFilter;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import imageConfig.imageSettings;
import imagechanger.ImageTimerManager;
import static imagechanger.ImageTimerManager.LOAD_TIMER;
import static imagechanger.ImageTimerManager.SAVE_TIMER;
import java.awt.image.BufferedImage;
/**
 *
 * @author Bartosz Hajdaś <bartekkh@gmail.com>
 * 
 */
public class imageFileReader extends JFileChooser{
    
    private String m_pathToFile;
    Image m_Image;
    File m_File;
    FileFilter imageFilter = null;
    

    public imageFileReader() {
        super();
        m_pathToFile = "";
        m_Image = null;
        imageFilter = new FileNameExtensionFilter("Image files", ImageIO.getReaderFileSuffixes());
  
    }
    
    public File getFile()
    {
        return m_File;
    }

    @Override
    public int showOpenDialog(Component parent) throws HeadlessException 
    {         
        File oFile = new File(imageSettings.getInstance().loadProperty(imageSettings.OPEN_DIALOG_BOX_PATH));        
        this.setCurrentDirectory(oFile); 
        this.setFileFilter(imageFilter);
        
        int retVal = 0; 
        
     
        retVal = super.showOpenDialog(parent);              
        
        if(0 == retVal)
        {
            m_File = this.getSelectedFile();
            if(null != m_File)
            {
                try
                {
                    m_pathToFile = m_File.getAbsolutePath();
                    imageSettings.getInstance().setProperty(imageSettings.OPEN_DIALOG_BOX_PATH, m_File.getParentFile().getAbsolutePath());
                    ImageTimerManager.getInstance().startTimer(LOAD_TIMER);
                    m_Image = ImageIO.read(m_File);
                    System.out.println(ImageTimerManager.getInstance().endTimer(LOAD_TIMER));
                }
                catch(IOException ex)
                {
                    ex.printStackTrace();
                }
            }            
        }
        
        return retVal;
    }


    public int showSaveDialog(Component parent, BufferedImage imgToSave) throws HeadlessException {
        File oFile = new File(imageSettings.getInstance().loadProperty(imageSettings.OPEN_DIALOG_BOX_PATH));        
        this.setCurrentDirectory(oFile); 
        this.setFileFilter(imageFilter);
        
        int retVal = 0; 
        
        retVal = super.showSaveDialog(parent);
        
        if(0 == retVal)
        {
            m_File = this.getSelectedFile();
            if(null != m_File)
            {
                try
                {
                    m_pathToFile = m_File.getAbsolutePath();
                    imageSettings.getInstance().setProperty(imageSettings.OPEN_DIALOG_BOX_PATH, m_File.getParentFile().getAbsolutePath());
                    ImageTimerManager.getInstance().startTimer(SAVE_TIMER);
                    ImageIO.write(imgToSave,"png", m_File);
                    ImageTimerManager.getInstance().endTimer(SAVE_TIMER);
                }
                catch(IOException ex)
                {
                    ex.printStackTrace();
                }
            }            
        }
        
        return retVal;
    }
    
    
    
    

    public String getpathToFile() {
        return m_pathToFile;
    }

    public Image getImage() {
        return m_Image;
    }
}
