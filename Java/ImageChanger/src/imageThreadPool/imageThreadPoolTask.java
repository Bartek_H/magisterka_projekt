/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageThreadPool;

/**
 *
 * @author Bartosz Hajdaś <bartekkh@gmail.com>
 */
public class imageThreadPoolTask implements  Runnable{

    int [] m_InputPixels = null;
    int [] m_OutputPixels = null;
    int m_width = 0;
    int m_weight = 0;
    int [][] m_imageFilter = null;
    int m_numberOfBorders = 0;
    int m_taskID = 0;
    
    public static final int ZERO_BORDER = 0;
    public static final int ONE_BORDER = 1;
    public static final int TWO_BORDERS = 2;
    
    
    public imageThreadPoolTask(int taskID, int numberOfBorders, int width, int weight, int [] pixels, int [][] imageFilter) 
    {
        this.m_weight = weight;
        this.m_width = width;
        this.m_taskID = taskID;
        this.m_InputPixels = pixels;
        // usuniecie gornej/dolnej krawedzi
        this.m_OutputPixels = new int[m_InputPixels.length - (numberOfBorders * width)];
        this.m_imageFilter = imageFilter;   
        this.m_numberOfBorders = numberOfBorders;
    }

    public int get_taskID() {
        return m_taskID;
    }
    
    public int [] getOutputPixels()
    {
        return m_OutputPixels;
    }
    
    private int changeColor(int mask, int shift, int i)
    {
        int color = ((m_InputPixels[i - 1 - m_width] & mask) >> shift) * m_imageFilter[0][0];
        color += ((m_InputPixels[i - m_width] & mask) >> shift) * m_imageFilter[0][1];
        color += ((m_InputPixels[i + 1 - m_width] & mask) >> shift) * m_imageFilter[0][2];

        color += ((m_InputPixels[i - 1] & mask) >> shift) * m_imageFilter[1][0];
        color += ((m_InputPixels[i] & mask) >> shift) * m_imageFilter[1][1];
        color += ((m_InputPixels[i + 1] & mask) >> shift) * m_imageFilter[1][2];       
        
        color += ((m_InputPixels[i - 1 + m_width ] & mask) >> shift) * m_imageFilter[2][0];
        color += ((m_InputPixels[i + m_width ] & mask) >> shift) * m_imageFilter[2][1];
        color += ((m_InputPixels[i + 1 + m_width ] & mask) >> shift) * m_imageFilter[2][2];
        
        return color;    
    }
    
    @Override
    public void run() {
        
        //odciecie u gory obrazka || odciecie u dolu obrazka
        boolean isAnyBorder = m_numberOfBorders > 0;
        int startPos = isAnyBorder ? m_width : 0;
        int endPos= isAnyBorder ? m_InputPixels.length - m_width : m_InputPixels.length ;
        int offset = isAnyBorder ? m_width : 0;
        
        for(int i = startPos; i < endPos; i++)
        {
            // gora dol obrazka 
            if(i < m_width || i >= m_InputPixels.length - m_width)
            {
                m_OutputPixels[i - offset] = m_InputPixels[i];
                continue;
            }
            //lewa strona obrazka || prawa strona obrazka
            if(((i % m_width) == 0) || (((i+1) % m_width) == 0))
            {
                m_OutputPixels[i - offset] = m_InputPixels[i];
                continue;
            }
            else
            {                
                int red = changeColor(0xFF0000, 16, i);
                int green = changeColor(0xFF00, 8, i);
                int blue = changeColor(0xFF, 0, i);

                if(m_weight != 0)
                {
                    red /= m_weight;
                    green /= m_weight;
                    blue /= m_weight;
                }
                if(red > 255) red = 255;
                if(green > 255) green = 255;
                if(blue > 255) blue = 255;
                if(red < 0) red = 0;
                if(green < 0) green = 0;
                if(blue < 0) blue = 0;

                m_OutputPixels[i - offset] = (0xFF << 24) + ((red) << 16) + ((green) << 8) + ((blue));
            }        
        }        
    }
}
