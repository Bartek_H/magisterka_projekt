/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageThreadPool;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bartosz Hajdaś <bartekkh@gmail.com>
 */
public class imageThreadPool {

    /**
     * Taks queue
     */
    private final LinkedBlockingQueue m_Queue;
    
    /**
     * number of threads
     */
    private final int m_Threads;
    
    /**
     * Thread pool
     */
    private final internalThreadWorker[] m_PoolThreads;
    /**
     * Flag that all jobs was ended
     */
    private final myObject m_HasThreadPoolEnded;

    /**
     * Ctor
     * @param threads used in thread pool
     */
    public imageThreadPool(int threads) {
        this.m_Threads = threads;
        this.m_PoolThreads = new internalThreadWorker[threads];
        this.m_Queue = new LinkedBlockingQueue();
        this.m_HasThreadPoolEnded = new myObject();

        for (int i = 0; i < m_Threads; i++) {
            m_PoolThreads[i] = new internalThreadWorker("InternalThreadWorker" + i, i);
            m_PoolThreads[i].start();
        }
    }

    public int getThreads() {
        return m_Threads;
    }
    
    

    /**
     * Add task to be done
     * @param task 
     */
    public void doTask(Runnable task) {
        if(null != task)
        {
            synchronized (m_Queue) {
                synchronized (m_HasThreadPoolEnded) {
                    m_HasThreadPoolEnded.incrementJobsAdded();
                }
                m_Queue.add(task);
                m_Queue.notify();
            }
        }
    }

    /**
     * Method called to wait for all task be excetued
     */
    public void waitForTaskDone() 
    {
        synchronized (m_HasThreadPoolEnded)
        {
            try 
            {
                m_HasThreadPoolEnded.wait();
                m_HasThreadPoolEnded.reset();
            } 
            catch (InterruptedException ex) 
            {
                Logger.getLogger(imageThreadPool.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private class myObject extends Object {

        private int m_JobsAdded;
        private int m_JobsEnded;

        public int getJobsAdded() {
            return m_JobsAdded;
        }

        public void incrementJobsAdded() {
            this.m_JobsAdded++;
        }

        public int getJobsEnded() {
            return m_JobsEnded;
        }

        public void incerementJobsEnded() {
            this.m_JobsEnded++;
        }
        
        public void reset()
        {
            this.m_JobsEnded = 0;
            this.m_JobsAdded = 0;
        }
    }

    private class internalThreadWorker extends Thread {

        Runnable m_InternalTask = null;
        private final int m_ThreadId;

        public internalThreadWorker(String threadName, int tid) {
            super(threadName);
            this.m_ThreadId = tid;
        }

        @Override
        public void run() {

            while (true) {
                synchronized (m_Queue) 
                {
                    while (m_Queue.isEmpty())
                    {
                        try 
                        {
                            m_Queue.wait();
                        } catch (InterruptedException ex)
                        {
                            ex.printStackTrace();
                        }
                    }
                    
                    m_InternalTask = (Runnable) m_Queue.poll();
                    

                }

                if (null != m_InternalTask) 
                {
                    System.out.println("task " + ((imageThreadPoolTask)m_InternalTask).get_taskID() + " started on thread " + m_ThreadId);
                    try 
                    {
                        sleep(5);
                    } 
                    catch (InterruptedException ex)
                    {
                        Logger.getLogger(imageThreadPool.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    try 
                    {
                        m_InternalTask.run();
                    } 
                    catch (RuntimeException e)
                    {
                        System.out.println("RuntimeException on thread " + m_ThreadId);
                        e.printStackTrace();
                    }
                    
                    System.out.println("task " + ((imageThreadPoolTask)m_InternalTask).get_taskID() + " ended on thread " + m_ThreadId);
                    
                    synchronized (m_HasThreadPoolEnded) 
                    {
                        m_HasThreadPoolEnded.incerementJobsEnded();
                        if (m_HasThreadPoolEnded.getJobsEnded() == m_HasThreadPoolEnded.getJobsAdded()) 
                        {
                            System.out.println("Thread pool ended work on TID " + m_ThreadId);
                            m_HasThreadPoolEnded.notify();                            
                        }
                    }

                }
            }
        }
    }
}
